package com.soa.productos.domain.service;

import com.soa.productos.domain.model.Producto;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DomainProductoService {

    List<Producto> getProductos(String parametro, Producto parametros);

    Producto getProducto(Producto producto);

    Producto verificarStockProducto(Producto producto);

    Producto actualizarProducto(Producto producto);

}
