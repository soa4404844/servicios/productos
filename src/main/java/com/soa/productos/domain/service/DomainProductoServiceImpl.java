package com.soa.productos.domain.service;

import com.soa.productos.domain.model.Producto;
import com.soa.productos.domain.repository.DomainProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DomainProductoServiceImpl implements DomainProductoService {

    @Autowired
    private DomainProductoRepository domainProductoRepository;


    @Override
    public List<Producto> getProductos(String parametro, Producto parametros) {
        return domainProductoRepository.findByPorParametro(parametro, parametros);
    }

    @Override
    public Producto getProducto(Producto producto) {
        return domainProductoRepository.findById(producto.getProductoID())
                .orElse(Producto.builder().build());
    }

    @Override
    public Producto verificarStockProducto(Producto producto) {
        return domainProductoRepository.verifyStock(producto)
                .orElse(Producto.builder().build());
    }

    @Override
    public Producto actualizarProducto(Producto producto) {
        var source = getProducto(producto);

        if (!source.getCantidad().equals(producto.getCantidad())) {
            source.setCantidad(producto.getCantidad());
        }

        return domainProductoRepository.save(source);
    }
}
