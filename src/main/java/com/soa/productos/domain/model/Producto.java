package com.soa.productos.domain.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "PRODUCTO")
public class Producto {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "producto_id")
    private Integer productoID;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "cantidad")
    private Double cantidad;

    @Column(name = "precio_compra")
    private Double precioCompra;

    @Column(name = "precio_venta")
    private Double precioVenta;

    @Column(name = "marca")
    private String marca;

    @Column(name = "fecha_caducidad")
    private OffsetDateTime fechaCaducidad;

    @Column(name = "fecha_compra")
    private OffsetDateTime fechaCompra;


}