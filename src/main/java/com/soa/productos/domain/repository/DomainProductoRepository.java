package com.soa.productos.domain.repository;

import com.soa.productos.domain.model.Producto;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DomainProductoRepository extends CrudRepository<Producto, Integer> {

    @Query("""
            SELECT P FROM PRODUCTO P
            WHERE (:#{#productoTabla.productoID} IS NULL OR P.productoID = :#{#productoTabla.productoID})
            AND (:parametro IS NULL
                OR CAST(P.nombre AS string) LIKE %:parametro%
                OR CAST(P.descripcion AS string) LIKE %:parametro%
                OR CAST(P.marca AS string) LIKE %:parametro%
            )
            ORDER BY P.fechaCompra DESC
            """)
    List<Producto> findByPorParametro(
            @Param("parametro") String parametro,
            @Param("productoTabla") Producto producto
    );

    @Query("""
            SELECT P FROM PRODUCTO P
            WHERE (:#{#productoTabla.productoID} IS NULL OR P.productoID = :#{#productoTabla.productoID})
            AND (:#{#productoTabla.cantidad} IS NULL OR P.cantidad >= :#{#productoTabla.cantidad})
            """)
    Optional<Producto> verifyStock(
            @Param("productoTabla") Producto producto
    );

}
