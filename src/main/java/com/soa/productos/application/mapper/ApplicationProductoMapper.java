package com.soa.productos.application.mapper;

import com.soa.productos.application.model.ObtenerProducto;
import com.soa.productos.domain.model.Producto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ApplicationProductoMapper {

    Producto obtenerProductoToProducto(ObtenerProducto obtenerProducto);

    ObtenerProducto productoToObtenerProducto(Producto producto);



}
