package com.soa.productos.application.service;


import com.soa.productos.application.api.ProductoApiDelegate;
import com.soa.productos.application.mapper.ApplicationProductoMapper;
import com.soa.productos.application.model.ObtenerProducto;
import com.soa.productos.domain.model.Producto;
import com.soa.productos.domain.service.DomainProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ApplicationProductoServiceImpl implements ProductoApiDelegate {

    @Autowired
    private ApplicationProductoMapper applicationProductoMapper;

    @Autowired
    private DomainProductoService domainProductoService;

    @Override
    public ResponseEntity<List<ObtenerProducto>> consultarProductos(String parametro, ObtenerProducto parametros) {
        var productos = domainProductoService.getProductos(
                parametro,
                applicationProductoMapper.obtenerProductoToProducto(parametros)
        );

        return ResponseEntity.ok(
                productos.stream()
                        .map(applicationProductoMapper::productoToObtenerProducto)
                        .toList()
        );
    }

    @Override
    public ResponseEntity<ObtenerProducto> obtenerProducto(Integer productoId) {

        var producto = domainProductoService.getProducto(Producto.builder()
                .productoID(productoId)
                .build()
        );

        return ResponseEntity.ok(
                applicationProductoMapper.productoToObtenerProducto(producto)
        );

    }

    @Override
    public ResponseEntity<ObtenerProducto> verificarStockProducto(Integer productoId,
                                                                  Double cantidad) {

        var producto = domainProductoService.verificarStockProducto(Producto.builder()
                .productoID(productoId)
                .cantidad(cantidad)
                .build()
        );

        return ResponseEntity.ok(
                applicationProductoMapper.productoToObtenerProducto(producto)
        );
    }

    @Override
    public ResponseEntity<ObtenerProducto> actualizarProducto(
            Integer productoId,
            ObtenerProducto parametros) {

        parametros.setProductoID(productoId);
        var producto = domainProductoService.actualizarProducto(
                applicationProductoMapper.obtenerProductoToProducto(parametros)
        );

        return ResponseEntity.ok(
                applicationProductoMapper.productoToObtenerProducto(producto)
        );
    }
}

